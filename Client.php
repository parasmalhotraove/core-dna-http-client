<?php
include './constants.php';
class Client
{
   /**
    * @param  $method
    * @param  $url
    * @param  $data
    * @param  $token
    * @return mixed
    */
   public static function callAPI($method, $url, $data,$token)
   {
      $curl = curl_init();
      switch ($method) {
         case REQUEST_METHOD_POST:
            curl_setopt($curl, CURLOPT_POST, 1);
            if ($data) {
               curl_setopt($curl, CURLOPT_POSTFIELDS, $data);          
            }
            break;
         case REQUEST_METHOD_PUT:
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, REQUEST_METHOD_PUT);
            if ($data) {
               curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }
            break;
         case REQUEST_METHOD_OPTIONS:
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, REQUEST_METHOD_OPTIONS);
            break;
         default:
            if ($data) {
               $url = sprintf("%s?%s", $url, http_build_query($data));
            }
      }
      // OPTIONS:
      curl_setopt($curl, CURLOPT_URL, $url);
      if($token){
         $headers = array(
            "Accept: application/json",
            "Authorization: Bearer {$token}",
            "Content-Type: application/json",
         );
      }else{
         $headers = array(
            'Content-Type: application/json',
         );
      }
      
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, false);
      curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      // EXECUTE:
      $result = curl_exec($curl);
      if (!$result) {
         die("Connection Failure");
      }
      curl_close($curl);
      return $result;
   }
}

