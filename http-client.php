<?php
//Get Request
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include './Client.php';
$authToken = Client::callAPI(REQUEST_METHOD_OPTIONS,ENDPOINT_URL,false,false);
$data = array(
    "name"=> USER_NAME,
    "email"=> USER_EMAIL,
    "url" => BITBUCKET_URL
);  
$make_call = Client::callAPI(REQUEST_METHOD_POST,ENDPOINT_URL,$data,$authToken);
if($make_call){
    echo 'Succesful submission complete';
}
?>
