<?php
/**
 * Contants decleared to be used in application scope
 */
if (!defined('GET_AUTH_TOKEN_REQUEST')) define('GET_AUTH_TOKEN_REQUEST', 'REQUEST_FOR_AUTH_TOKEN');
if (!defined('REQUEST_METHOD_POST')) define('REQUEST_METHOD_POST', 'POST');
if (!defined('REQUEST_METHOD_GET')) define('REQUEST_METHOD_GET', 'GET');
if (!defined('REQUEST_METHOD_PUT')) define('REQUEST_METHOD_PUT', 'PUT');
if (!defined('REQUEST_METHOD_DELETE')) define('REQUEST_METHOD_DELETE', 'DELETE');
if (!defined('REQUEST_METHOD_OPTIONS')) define('REQUEST_METHOD_OPTIONS', 'OPTIONS');
if (!defined('ENDPOINT_URL')) define('ENDPOINT_URL', 'https://corednacom.corewebdna.com/assessment-endpoint.php');
if (!defined('USER_NAME')) define('USER_NAME', 'Paras Malhotra');
if (!defined('USER_EMAIL')) define('USER_EMAIL', 'paras.malhotra@optimalvirtualemployee.com');
if (!defined('BITBUCKET_URL')) define('BITBUCKET_URL', 'https://bitbucket.org/parasmalhotraove/core-dna-http-client')
?>
